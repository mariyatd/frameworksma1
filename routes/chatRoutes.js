var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var routes = function(Chatroom){

    var chatRouter = express.Router();




    chatRouter.route('/chatroom')
        .post(function(req, res){
            var newChatroom = {
                "name" : req.body.newChatroomName,
                "messages" : []
            };
            var chat = new Chatroom(newChatroom);

            console.log(chat);

            //adding the chat to the db
            chat.save();

            res.status(201);
        })
        .get(function(req, res){
            Chatroom.find(function(err, chatrooms){
                if(err){
                    res.status(500).send(err);
                }
                else {
                    //res.json(chatrooms);
                    res.render('chatroom', {chatz: chatrooms});
                }
            });

        });

    chatRouter.route('/chatroom/:chatId')
        .get(function(req, res){

            Chatroom.findById(req.params.chatId, function(err, chat){
                if(err){
                    res.status(500).send(err);
                }
                else {
                    //res.json(chat);

                    res.render('chat', chat);
                }
            });
        })
        //.post(function(req, res){
        //    //var arr = {"message":"bleh","sender":"aaaa"};
        //        Chatroom.findById(req.params.chatId, function(err, chat){
        //        if(err){
        //            res.status(500).send(err);
        //        }
        //        else {
        //            var responseObj = {"message": req.body.newMessage,
        //                "sender": req.body.messageSender};
        //            //console.log(responseObj);
        //            chat.messages.push( responseObj );
        //            chat.save();
        //
        //            io.on('connection', function (socket) {
        //                socket.emit('news', { hello: 'world' });
        //                socket.on('my other event', function (data) {
        //                    console.log(data);
        //                });
        //            });
        //        }
        //    }
        //
        //
        //    );
        //})
    ;
    return chatRouter;

};

module.exports = routes;