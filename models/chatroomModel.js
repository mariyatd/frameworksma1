var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var chatroomModel = new Schema({
    name: String,
    messages: [{
        message: String,
        sender: String
    }]
});

module.exports = mongoose.model('Chatroom', chatroomModel);