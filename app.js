var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var path = require('path');
var bodyParser = require('body-parser');

var port = 8080;
var socketPort = 8888;

//The mongoose stuff here
var mongoose = require('mongoose');

//Connect to MongoDB
var db = mongoose.connect('mongodb://root:root@ds135577.mlab.com:35577/maframeworksmariya');
var Chatroom = require('./models/chatroomModel');

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

chatRouter = require('./Routes/chatRoutes.js')(Chatroom);

app.use('/', chatRouter);


//Serving static files from the folder "public":
    app.set('view engine', 'jade');
    app.set('views', __dirname + '/views');
    app.use(express.static(path.join(__dirname, "public")));

//Routes

io.on('connection', function(socket) {
    console.log('socket.io connected');
});

io.on('data', function(data) {
    console.log('nqnqnqnq');
    Chatroom.findById(data.idid, function(err, chat) {
            if (err) {
                res.status(500).send(err);
            }
            else {
                var responseObj = {
                    "message": data.newMessage,
                    "sender": data.messageSender
                };
                //console.log(responseObj);
                chat.messages.push(responseObj);
                chat.save(function(err) {
                    if(err) { return console.log("error"); }
                    socket.emit('callback', {done: 'Done', data: data});
                });
            }
        }
    );



    //new Chat({
    //    routeFrom : data.routeFrom,
    //    routeTo : data.routeTo,
    //    leaving: data.leaving
    //}).save
});

server.listen(port, function () {
    console.log('MA1 chat app: listening on port ' + port);
});